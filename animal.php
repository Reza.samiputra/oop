<?php 

class Animal 
{
    public $name;
    public $leg = 2,
            $cold_blooded = "false";

    function __construct($nama)
    {
     $this->name = $nama;
    }
    function getName()
     {
         return $this -> name;
     }
     function getLeg()
     {
         return $this-> leg;
     }
     function get_cold_blooded()
     {
         return $this-> cold_blooded;
     }
}


